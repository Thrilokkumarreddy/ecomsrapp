package com.nit.ecomrs.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nit.ecomrs.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
