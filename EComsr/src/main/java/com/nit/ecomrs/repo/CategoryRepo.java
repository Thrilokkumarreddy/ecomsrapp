package com.nit.ecomrs.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nit.ecomrs.entity.Category;

public interface CategoryRepo extends JpaRepository<Category, Long> {

}
