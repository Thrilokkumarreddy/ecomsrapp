package com.nit.ecomrs.service;

import java.util.List;
import java.util.Map;

import com.nit.ecomrs.entity.Brand;

public interface BrandService {

	Long saveBrand(Brand b);
	void updateBrand(Brand b);
	void deleteBrand(Long id);
	Brand getOneBrand(Long id);
	List<Brand> getAllBrands();
	Map<Integer,String> getBrandIdAndName();

}
