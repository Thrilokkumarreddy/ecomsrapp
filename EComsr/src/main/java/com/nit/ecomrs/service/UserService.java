package com.nit.ecomrs.service;

import com.nit.ecomrs.entity.User;

public interface UserService {
	
	public Long saveUser(User user);
	
}
