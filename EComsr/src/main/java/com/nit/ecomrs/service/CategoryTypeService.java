package com.nit.ecomrs.service;

import java.util.List;
import java.util.Map;

import com.nit.ecomrs.entity.CategoryType;

public interface CategoryTypeService{

	public Long saveCategoryType(CategoryType category);
	public List<CategoryType> getAllCategoryType();
	public void updateCategoryTypeById(CategoryType categoryType);
	public void deleteCategoryTypeById(Long id);
	public CategoryType getOneCategoryTypeById(Long id);
	Map<Integer, String> getCategoryTypeIdAndName();
}
