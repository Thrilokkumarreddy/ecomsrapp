package com.nit.ecomrs.service;

import java.util.List;

import com.nit.ecomrs.entity.Category;

public interface CategoryService {

	public Long saveCategory(Category category);
	public List<Category> getAllCategory();
	public void updateCategoryById(Category category);
	public void deleteCategoryById(Long id);
	public Category getCategoryById(Long id);
	//void updateCategoryById(Long id);
}
