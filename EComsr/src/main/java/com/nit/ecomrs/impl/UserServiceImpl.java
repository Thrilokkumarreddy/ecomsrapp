package com.nit.ecomrs.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nit.ecomrs.entity.User;
import com.nit.ecomrs.repo.UserRepository;
@Service
public class UserServiceImpl implements com.nit.ecomrs.service.UserService {

	@Autowired
	private UserRepository repo;

	@Override
	public Long saveUser(User user) {
		return repo.save(user).getUId();
	}

}
