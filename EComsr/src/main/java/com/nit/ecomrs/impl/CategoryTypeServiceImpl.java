package com.nit.ecomrs.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nit.ecomrs.Exception.CategoryNotFoundException;
import com.nit.ecomrs.entity.CategoryType;
import com.nit.ecomrs.repo.CategoryTypeRepository;
import com.nit.ecomrs.service.CategoryTypeService;
import com.nt.ecomrs.util.AppUtil;

@Service
public class CategoryTypeServiceImpl implements CategoryTypeService {

	@Autowired
	private CategoryTypeRepository repo;

	@Override
	public Long saveCategoryType(CategoryType categoryType) {
		return repo.save(categoryType).getId();
	}

	@Override
	public List<CategoryType> getAllCategoryType() {
		return repo.findAll();
	}

	@Override
	public void updateCategoryTypeById(CategoryType categoryType) {
		repo.save(categoryType);
	}

	@Override
	public void deleteCategoryTypeById(Long id) {
		repo.delete(getOneCategoryTypeById(id));
	}

	@Override
	public CategoryType getOneCategoryTypeById(Long id) {
		Optional<CategoryType> optional = repo.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		} else {
			throw new CategoryNotFoundException(id + "Not Exist");
		}
	}

	@Override
	public Map<Integer, String> getCategoryTypeIdAndName() {
		List<Object[]> list = repo.getCategoryTypeIdAndName();
		Map<Integer, String> map = AppUtil.listToMapConversion(list);
		return map;
	}
}
