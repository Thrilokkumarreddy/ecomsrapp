package com.nit.ecomrs.Exception;

public class CategoryNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 4872042595934123949L;

	public CategoryNotFoundException() {
		super();
	}
	
	public CategoryNotFoundException(String message) {
		super(message);
	}
}
