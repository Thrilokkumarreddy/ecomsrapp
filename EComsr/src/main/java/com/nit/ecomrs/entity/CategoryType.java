package com.nit.ecomrs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cat_type_tab")
@Data
public class CategoryType {

	@Id
	@Column(name = "cat_type_id_col")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "cat_type_name_col")
	private String name;

	@Column(name = "cat_type_note_col")
	private String note;

}
