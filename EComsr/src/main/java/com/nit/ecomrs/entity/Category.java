package com.nit.ecomrs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cat_tab")
public class Category {

	@Id
	@Column(name = "cat_cid_col")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "cat_name_col")
	private String name;
	
	@Column(name = "cat_alias_col")
	private String alias;
	
	@Column(name = "cat_note_col")
	private String note;
	
	@Column(name = "cat_status_col")
	private String status;
	
	/* Integration */
	@ManyToOne
	@JoinColumn(name="cat_type_fk_col")
	private CategoryType categoryType;
	
}
