package com.nt.ecomrs.util;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

@Component
public class AppUtil {

	public static Map<Integer,String> listToMapConversion(List<Object[]> list){
		return list.stream()
											.collect(
															Collectors.toMap(
																			ob->Integer.parseInt(ob[0].toString())
																			,ob->ob[1].toString()));
		}
}
